CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration



INTRODUCTION
------------
This module provides various low-level API functions for Drupal Core taxonomy.
This module is only needed if a module you want to install lists it as a dependency.



REQUIREMENTS
------------
This module requires the core taxonomy module.



INSTALLATION
------------
Install as you would normally install a contributed Drupal module.
See: https://drupal.org/documentation/install/modules-themes/modules-7
for further information.



CONFIGURATION
-------------
This module has no configuration options.

